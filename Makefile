all: test.pdf

test.pdf: $(wildcard beamer*theme*.sty)

view: all
	gnome-open test.pdf

.tex.pdf: $<
	pdflatex $< && rm $@ && pdflatex $<

.SUFFIXES: .tex .pdf

distclean: clean
	$(RM) test.pdf

clean:
	$(RM) *.aux *.log *.nav *.out *.snm *.toc
